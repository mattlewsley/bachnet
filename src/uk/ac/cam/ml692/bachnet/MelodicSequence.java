package uk.ac.cam.ml692.bachnet;

import java.util.*;

public class MelodicSequence {

    /**
     * Integer represents beat in piece. Each beat has a LinkedList of Notes (which could be empty)
     */
    private TreeMap<Integer, LinkedList<Note>> notes;

    /**
     * Constructor
     * Instantiates empty LinkedList notes
     */
    public MelodicSequence() {
        notes = new TreeMap<>();
    }


    /**
     * Construct a melodic sequence from an existing collection of notes
     * @param list
     */
    public MelodicSequence(TreeMap<Integer, LinkedList<Note>> list) {
        notes = list;
    }

    // TODO: What if the beat already contains data?
    /**
     * Adds the notes to the specified beat position
     * @param notesToAdd
     * @param position
     */
    public void addNotes(LinkedList<Note> notesToAdd, int position) {
        notes.put(position, notesToAdd);
    }

    /**
     * Adds a note to the end of the sequence
     * @param notesToAdd
     */
    public void addNotes(LinkedList<Note> notesToAdd) {
        int beatToAdd = notes.lastKey() + 1;
        notes.put(beatToAdd, notesToAdd);
    }

    /**
     * Returns all notes in the sequence
     */
    public TreeMap<Integer, LinkedList<Note>> getNotes() {
        return notes;
    }

    // TODO: Check for nulls + empty lists??
    /**
     * Checks if the sequence has any data at the specified index
     * @param index
     * @return
     */
    public boolean hasNoteAtIndex(int index) {
        return notes.containsKey(index);
    }


    // TODO: What if the note doesn't exist?
    /**
     * Returns the note at the specified index
     * @param index
     * @return Note
     */
    public LinkedList<Note> getNote(int index) {
        return notes.get(index);
    }

    /**
     * Return the highest beat that we have data for
     * @return
     */
    public int getHighestBeat() {
        return notes.lastKey();
    }

    /**
     * Transpose the sequence down by the specified amount
     * @param shiftBy
     */
    public void shift(int shiftBy) {
        for (Map.Entry<Integer, LinkedList<Note>> entry : notes.entrySet()) {
            LinkedList<Note> notes = entry.getValue();
            for (Note note : notes) {
                if (note.getName() != Note.NoteName.REST) { // Don't shift rests
                    note.shift(shiftBy);
                }
            }
        }
    }

}