package uk.ac.cam.ml692.bachnet;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Note {

    // Enumeration for note name allows easy mapping from conventional note names to integer representation
    public enum NoteName {

        // Define notes
        C(0), CSharp(1), D(2), DSharp(3), E(4), F(5), FSharp(6), G(7), GSharp(8), A(9), ASharp(10), B(11), REST(12);

        // Stores integer representation
        private int value;

        // Constructor initialises value field
        NoteName(int value) {
            this.value = value;
        }

        // Map to enable reverse lookup (to get a NoteName from a value)
        private static final Map<Integer,NoteName> lookup = new HashMap<Integer,NoteName>();

        // Populates the lookup map
        static {
            for(NoteName nn: EnumSet.allOf(NoteName.class))
                lookup.put(nn.getValue(), nn);
        }

        // Method to turn NoteName into a Neural Network double[] input
        public double[] toNeuralNetworkInput() {
            double[] output = new double[NoteName.values().length-1];
            if (this.getValue() != 12) { // Don't set any bits if it's a rest
                output[this.getValue()] = 1;
            }
            return output;
        }

        public int getValue() { return value; }

        public static NoteName get(int value) {
            return lookup.get(value);
        }
    }

    // Treats semi-quavers (1/4 beat) as the shortest possible notes, and semi-breaves as the longest possible notes (4 beats)
    // Methods are largely similar to NoteName
    public enum NoteLength {

        SQ(1), Q(2), DQ(3), C(4), DC(6), M(8), DM(12), SB(16);
        private int value;

        NoteLength(int value) {
            this.value = value;
        }

        private static final Map<Integer,NoteLength> lookup
                = new HashMap<Integer,NoteLength>();

        static {
            for(NoteLength nn: EnumSet.allOf(NoteLength.class))
                lookup.put(nn.getValue(), nn);
        }

        public int getValue() { return value; }

        public static NoteLength get(int value) {
            if (lookup.containsKey(value)) {
                return lookup.get(value);
            } else {
                // Assume note is shorter than semi-quaver
                return SQ;
            }
        }
    }

    // Fields

    private NoteName name;
    private NoteLength length;

    // Ranges from 0 to 9, with middle C at 4
    private Integer octave;

    /**
     * Constructor
     * @param n - note name
     * @param l - note length
     * @param o - octave
     */
    public Note(NoteName n, NoteLength l, Integer o) {
        name = n;
        length = l;
        octave = o;
    }

    /**
     * Constructor to generate a note from an integer pitch instead of a NoteName and octave int
     * @param pitch
     * @param l
     */
    public Note(int pitch, NoteLength l) {
        octave = pitch / 12;
        int nameValue = pitch % 12;
        name = Note.NoteName.get(nameValue);
        length = l;
    }

    /**
     * Shift note by a random interval (for GA)
     * @param random
     */
    public void shift(Random random) {

        Note.NoteName[] names = {Note.NoteName.C, Note.NoteName.E, Note.NoteName.G};

        int newValue = random.nextInt(3);

        this.name = names[newValue];

    }

    /**
     * Shift note down by specified amount (for NN normalisation)
     * @param shiftBy
     */
    public void shift(int shiftBy) {
        int newValue = this.getName().getValue() - shiftBy;

        if (newValue <0) {
            newValue += 12;
            octave--;
        }

        this.name = NoteName.get(newValue);
    }

    public boolean isRest() {
        return this.name == NoteName.REST;
    }

    public NoteName getName() {
        return name;
    }

    public NoteLength getLength() {
        return length;
    }

    public int getOctave() {
        return octave;
    }

    /**
     * Returns the integer pitch value (needed for MIDI encoding) of the Note
     * @return
     */
    public int getPitch() {
        return (octave * 12) + name.getValue();
    }

}