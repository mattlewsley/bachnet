package uk.ac.cam.ml692.bachnet.genetic;

import org.uncommons.watchmaker.framework.FitnessEvaluator;
import uk.ac.cam.ml692.bachnet.*;

import java.sql.Time;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Fitness evaluator for the genetic algorithm
 */

// TODO : Factor out fitness rules to a FitnessRule object which can be collected here

public class ChoraleHarmonyFitnessEvaluator implements FitnessEvaluator<Chorale> {

    /**
     * Get the fitness score for this chorale
     * @param chorale -- current candidate being evaluated
     * @param list -- population
     * @return double -- fitness score
     */
    @Override
    public double getFitness(Chorale chorale, List<? extends Chorale> list) {

        double chordFitness = getChordFitness(chorale);
        double chordProgressionFitness = getChordProgressionFitness(chorale);
        double openingChordFitness = getOpeningChordFitness(chorale);
        double closingChordFitness = getClosingChordFitness(chorale);

        double combinedFitness = chordFitness + chordProgressionFitness + openingChordFitness + closingChordFitness;

        return Math.max(combinedFitness, 0);

    }

    /**
     * Assign fitness score to the Chord/Melody allocations in the Chorale
     * @param chorale -- input chorale
     * @return fitness
     */
    private double getChordFitness(Chorale chorale) {

        double fitness = 0;

        MelodicSequence melodicSequence = chorale.getSopranoLine();
        HarmonicSequence harmonicSequence = chorale.getHarmonies();

        for (Map.Entry<Integer, LinkedList<Note>> entry : melodicSequence.getNotes().entrySet()) {
            int beat = entry.getKey();
            LinkedList<Note> notes = melodicSequence.getNote(beat);
            fitness += ChordRankings.getRanking(harmonicSequence.getHarmony(beat), notes.getFirst().getName());
        }

        return fitness;

    }

    /**
     * Assign fitness score to the chord progressions in the Chorale
     * @param chorale -- input chorale
     * @return fitness
     */
    private double getChordProgressionFitness(Chorale chorale) {

        double fitness = 0;

        HarmonicSequence harmonicSequence = chorale.getHarmonies();

        for (Map.Entry<Integer, Harmony> entry : harmonicSequence.getHarmonies().entrySet()) {

            int beat = entry.getKey();

            if (harmonicSequence.containsBeat(beat - 1)) {
                Harmony thisHarmony = entry.getValue();
                Harmony previousHarmony = harmonicSequence.getHarmony(beat - 1);
                fitness += ChordProgressionRankings.getRanking(previousHarmony, thisHarmony);
            }

        }

        return fitness;

    }

    /**
     * Checks whether or not the opening Chord is C Major as expected
     * If no chord at beat 0, it checks the next bar
     * @param chorale -- input chorale
     * @return -- score - 0 if not the right chord
     */
    private double getOpeningChordFitness(Chorale chorale) {

        // Scores to award
        double goodScore = 500d;
        double badScore = 0d;

        HarmonicSequence harmonicSequence = chorale.getHarmonies();

        Harmony CMajor = new Harmony(Note.NoteName.C, Harmony.Inversion.A, EnumSet.noneOf(Harmony.Dissonance.class), Harmony.Tonality.MAJOR);

        int firstBeat = harmonicSequence.lowestBeat();

        if (firstBeat == 0) {
            return (harmonicSequence.getHarmony(firstBeat).equals(CMajor)) ? goodScore : badScore;
        } else {

            // Try and find first beat
            TimeSignature timeSignature = chorale.getTimeSignature();
            int top = timeSignature.getTop();

            return (harmonicSequence.getHarmony(top).equals(CMajor)) ? goodScore : badScore;

        }

    }

    /**
     * Checks whether or not the closing chord is a tonic chod
     * @param chorale -- chorale to check
     * @return -- s
     */
    private double getClosingChordFitness(Chorale chorale) {

        // Scores to award
        double goodScore = 500d;
        double badScore = 0d;

        HarmonicSequence harmonicSequence = chorale.getHarmonies();

        Harmony CMajor = new Harmony(Note.NoteName.C, Harmony.Inversion.A, EnumSet.noneOf(Harmony.Dissonance.class), Harmony.Tonality.MAJOR);

        int lastBeat = harmonicSequence.highestBeat();

        return (harmonicSequence.getHarmony(lastBeat).equals(CMajor)) ? goodScore : badScore;

    }

    /**
     * Determines whether or not this fitness score is natural
     * If true -- higher score is better
     * @return true/false
     */
    @Override
    public boolean isNatural() {
        return true;
    }

}
