
package uk.ac.cam.ml692.bachnet.genetic;

import org.uncommons.watchmaker.framework.operators.AbstractCrossover;
import uk.ac.cam.ml692.bachnet.Chorale;
import uk.ac.cam.ml692.bachnet.HarmonicSequence;
import uk.ac.cam.ml692.bachnet.Harmony;

import java.util.*;


/**
 * Chorale crossover operator for GA
 */

public class ChoraleCrossover extends AbstractCrossover<Chorale> {

    /**
     * Constructor for a ChoraleCrossover operator
     * @param crossoverPoints - number of crossover points to use
     */
    protected ChoraleCrossover(int crossoverPoints) {
        super(crossoverPoints);
    }

    /**
     * Mates two chorales together
     * @param t1 - first chorale
     * @param t2 - second chorale
     * @param numberOfCrossoverPoints - number of crossover points to use
     * @param rng - PRNG
     * @return - List of mated Chorales
     */
    @Override
    protected List<Chorale> mate(Chorale t1, Chorale t2, int numberOfCrossoverPoints, Random rng) {

        // Create offspring chorales
        Chorale offspring1 = new Chorale();
        Chorale offspring2 = new Chorale();

        // Add melody to both
        offspring1.addSopranoLine(t1.getSopranoLine());
        offspring2.addSopranoLine(t2.getSopranoLine());

        HarmonicSequence offspringHarmonies1 = t1.getHarmonies().clone();
        HarmonicSequence offspringHarmonies2 = t2.getHarmonies().clone();

        for (int i = 0; i < numberOfCrossoverPoints; i++) {

            // Generate random crossover index -- has to be at least 1 bar in and 1 bar before the end of the piece
            int crossoverIndex = (4 + rng.nextInt(t1.getSopranoLine().getHighestBeat() - 4));

            if (offspringHarmonies1.highestBeat() != offspringHarmonies2.highestBeat()) {
                throw new IllegalArgumentException("Chorale harmonies are not of equal length");
            }

            for (int beat = 0; beat <= offspringHarmonies1.highestBeat(); beat++) {
                if (offspringHarmonies1.containsBeat(beat) && offspringHarmonies2.containsBeat(beat)) {
                    if (beat <= crossoverIndex) {
                        Harmony harmony1 = offspringHarmonies1.getHarmony(beat);
                        Harmony harmony2 = offspringHarmonies2.getHarmony(beat);
                        offspringHarmonies1.addHarmony(beat, harmony2);
                        offspringHarmonies2.addHarmony(beat, harmony1);
                    }
                }
            }

        }

        offspring1.setHarmonies(offspringHarmonies1);
        offspring2.setHarmonies(offspringHarmonies2);

        List<Chorale> outputs = new ArrayList<>(2);

        outputs.add(offspring1);
        outputs.add(offspring2);

        return outputs;

    }
}