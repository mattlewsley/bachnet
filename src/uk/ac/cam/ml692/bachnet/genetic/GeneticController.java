package uk.ac.cam.ml692.bachnet.genetic;

import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.selection.SigmaScaling;
import org.uncommons.watchmaker.framework.selection.TournamentSelection;
import org.uncommons.watchmaker.framework.termination.Stagnation;
import org.uncommons.watchmaker.framework.termination.TargetFitness;
import uk.ac.cam.ml692.bachnet.*;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * To be used as an entry point for the genetic application
 */
public class GeneticController {

    /**
     * Initialise ranking controllers
     */
    private static void initialiseRankings() {
        ChordRankings.loadRankings();
        ChordProgressionRankings.loadRankings();
    }

    /**
     * Calculate a Chorale solution from the specified path using the GA
     * @param filePath -- input MIDI file
     * @return Chorale -- result
     * @throws InvalidMidiDataException
     * @throws IOException
     */
    private static Chorale calculateFromFile (String filePath) throws InvalidMidiDataException, IOException {

        initialiseRankings();

        Sequence sequence = MidiSystem.getSequence(new File(filePath));
        Chorale seed = new Chorale(sequence);
        seed.normalise();

        CandidateFactory<Chorale> factory = new ChoraleFactory(seed);

        List<EvolutionaryOperator<Chorale>> operators
                = new LinkedList<>();

        operators.add(new ChoraleHarmonyMutator(new Probability(0.5), seed.getSopranoLine().getHighestBeat()));
        operators.add(new ChoraleCrossover(3));

        EvolutionaryOperator<Chorale> pipeline
                = new EvolutionPipeline<>(operators);

        FitnessEvaluator<Chorale> fitnessEvaluator = new ChoraleHarmonyFitnessEvaluator();

        SelectionStrategy<Object> selection = new RouletteWheelSelection();

        Random rng = new MersenneTwisterRNG();

        EvolutionEngine<Chorale> engine
                = new GenerationalEvolutionEngine<>(factory,
                pipeline,
                fitnessEvaluator,
                selection,
                rng);

        engine.addEvolutionObserver(GeneticController::printGenerationData);

        // Starting population, elite count, termination condition

        return engine.evolve(10000, 0, new Stagnation(50, true));

    }

    /**
     * Helper function to print meta data about a generation to the console
     * @param populationData - data to print
     */
    private static void printGenerationData(PopulationData populationData) {

        System.out.println("Generation " + populationData.getGenerationNumber() + " | "
                + " Mean Fitness " + populationData.getMeanFitness() + " | "
                + " Fitness STD Dev " + populationData.getFitnessStandardDeviation() + " | "
                + " Elite Count " + populationData.getEliteCount() + " | "
                + " Best Fitness " + populationData.getBestCandidateFitness() + " | "
                + " Population Size " + populationData.getPopulationSize());

    }

    /**
     * Prints information about the validity of this solution to console
     * @param chorale -- chorale to analyse
     */
    public static int analyseChorale(Chorale chorale) {

        HarmonicSequence harmonicSequence = chorale.getHarmonies();
        MelodicSequence sopranoLine = chorale.getSopranoLine();
        ChordRankings.loadRankings();

        System.out.println("Opening chord at beat " + harmonicSequence.lowestBeat()
                + " is " + harmonicSequence.getHarmony(harmonicSequence.lowestBeat()).toString());

        System.out.println("Closing chord at beat " + harmonicSequence.highestBeat()
                + " is " + harmonicSequence.getHarmony(harmonicSequence.highestBeat()).toString());

        int penalty = 0;
        int progressions = 0;

        for (Map.Entry<Integer, Harmony> entry : harmonicSequence.getHarmonies().entrySet()) {

            int beat = entry.getKey();

            if (sopranoLine.hasNoteAtIndex(beat)) {

                LinkedList<Note> notes = sopranoLine.getNote(beat);

                if (notes.getFirst().getName() != Note.NoteName.REST) {
                    Harmony harmony = entry.getValue();
                    int score = ChordRankings.getRanking(harmony, notes.getFirst().getName());
                    if (score != 40) {
                        penalty += (score - 40);
                        System.out.println("Beat " + beat + " has an imperfect match of " + notes.getFirst().getName() + " with " + harmony.getRootNote() + " " + harmony.getTonality());
                    }
                    if (harmonicSequence.containsBeat(beat+1)) {
                        int progressionToNext = ChordProgressionRankings.getRanking(harmony, harmonicSequence.getHarmony(beat+1));
                        if (progressionToNext > 0) {
                            System.out.println("Good progression from " + harmony.getRootNote() + " " + harmony.getTonality()
                            + " to " + harmonicSequence.getHarmony(beat+1).getRootNote() + " " + harmonicSequence.getHarmony(beat+1).getTonality());
                        }
                    }
                }
            }

        }

        System.out.println("Total Penalty: " + penalty);
        System.out.println("Progression Score: " + progressions);

        return penalty;

    }

    public static void main(String[] args) throws InvalidMidiDataException, IOException {

        Chorale fittest = new Chorale();
        int penalty = -100000;

        for (int i = 0; i < 1; i++) {
            Chorale result = calculateFromFile("/Users/mattlewsley/Documents/BachNet/TrainingData/030300B.midi");
            int thisPenalty = analyseChorale(result);
            if (thisPenalty > penalty) {
                fittest = result;
                penalty = thisPenalty;
            }
        }

        System.out.println("Best penalty : " + penalty);

        fittest.harmonicSequenceToMelodicSequences();

        fittest.addPassingNotes(0.75);

        MIDI.writeChoraleToMIDI(fittest, "/Users/mattlewsley/Documents/BachNet/temp/MyOutputFile.mid");

    }

}
