
package uk.ac.cam.ml692.bachnet.genetic;

import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;
import uk.ac.cam.ml692.bachnet.*;

import java.util.*;


/**
 * Factory for creating random Chorale solutions to seed GA with
 */

public class ChoraleFactory extends AbstractCandidateFactory<Chorale> {

    /**
     * Original chorale for melody line preservation
     */
    Chorale inputChorale;

    /**
     * Constructor
     * @param chorale -- input chorale for melody line preservation
     */
    public ChoraleFactory(Chorale chorale) {
        inputChorale = chorale;
    }

    /**
     * Function for creating a random solution when seeding the GA
     * Fills out every beat of harmony with a random chord
     * @param random -- PRNG
     * @return -- random chorale
     */
    @Override
    public Chorale generateRandomCandidate(Random random) {

        MelodicSequence sopranoLine = inputChorale.getSopranoLine();

        Chorale randomChorale = new Chorale();
        randomChorale.addSopranoLine(sopranoLine);

        EnumSet<Harmony.Dissonance> dissonances = EnumSet.noneOf(Harmony.Dissonance.class);
        Harmony.Inversion inversion = Harmony.Inversion.A;

        HarmonicSequence harmonicSequence = new HarmonicSequence();

        for (Map.Entry<Integer, LinkedList<Note>> entry : inputChorale.getSopranoLine().getNotes().entrySet()) {

            int beat = entry.getKey();

            if (entry.getValue().getFirst().getName() != Note.NoteName.REST) {

                // Random note between 0 (inclusive) and 12 (exclusive)
                Note.NoteName rootNote = Note.NoteName.get(random.nextInt(12));

                Harmony.Tonality tonality = (random.nextBoolean()) ? Harmony.Tonality.MAJOR : Harmony.Tonality.MINOR;

                Harmony harmony = new Harmony(rootNote, inversion, dissonances, tonality);

                harmonicSequence.addHarmony(beat, harmony);

            }
        }

        randomChorale.setHarmonies(harmonicSequence);

        return randomChorale;

    }
}