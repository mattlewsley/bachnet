package uk.ac.cam.ml692.bachnet.genetic;

import uk.ac.cam.ml692.bachnet.Harmony;
import uk.ac.cam.ml692.bachnet.Note;

/**
 * Represents a chord progression of two chords
 * Used as a key into ChordProgressionRankings
 */
public class ChordProgression {

    /**
     * Chord members
     */
    private Harmony firstChord;
    private Harmony secondChord;

    /**
     * Constructor for a chord progression
     * @param firstChord - first chord in progression
     * @param secondChord - second chord in progression
     */
    public ChordProgression(Harmony firstChord, Harmony secondChord) {
        this.firstChord = firstChord;
        this.secondChord = secondChord;
    }

    /**
     * Get the first chord
     * @return Harmony
     */
    public Harmony getFirstChord() {
        return firstChord;
    }

    /**
     * Get the second chord
     * @return Harmony
     */
    public Harmony getSecondChord() {
        return secondChord;
    }

    /**
     * Equality test for HashMap
     * @param o -- object to compare with
     * @return -- true/false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChordProgression)) return false;
        ChordProgression progression = (ChordProgression) o;
        return (progression.getFirstChord().equals(this.firstChord) && progression.getSecondChord().equals(this.secondChord));
    }

    /**
     * Generate a hash code for this progression for use in HashMap
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return firstChord.getRootNote().hashCode()
                + firstChord.getTonality().hashCode()
                + secondChord.getRootNote().hashCode()
                + secondChord.getTonality().hashCode();
    }

}
