package uk.ac.cam.ml692.bachnet.genetic;

import uk.ac.cam.ml692.bachnet.Harmony;
import uk.ac.cam.ml692.bachnet.Note;
import uk.ac.cam.ml692.bachnet.Note.NoteName;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Handles import and maintenance of melody-chord mapping rankings
 */

public class ChordRankings {

    /**
     * HashMap of rankings
     */
    private static HashMap<NoteHarmonyMapping, Integer> rankings = new HashMap<>();

    /**
     * Mapping from text file encoding of scores to internal scores for fine-tweaking
     * [Bad, Average, Good]
     */
    private static int[] scores = {0, 15, 40};

    /**
     * Load the rankings from a txt config file
     */
    public static void loadRankings() {
        URL url = ChordRankings.class.getClassLoader().getResource("files/chordRankings.txt");
        if (url != null) {
            try (Stream<String> stream = Files.lines(Paths.get(url.getFile()), Charset.defaultCharset())) {
                stream.forEach(ChordRankings::addToRankings);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Helper function for loadRankings()
     * @param fileLine -- String representing one line of input file
     */
    private static void addToRankings(String fileLine) {
        String[] parts = fileLine.split("\\s+");
        if (Objects.equals(parts[0], "#")) {
            System.out.println("Skipping line -- " + fileLine);
            return;
        }
        if (parts.length != 4) {
            System.out.println("Invalid syntax in chord rankings file -- " + fileLine);
            return;
        }
        try {
            Note.NoteName melodyNote = Note.NoteName.valueOf(parts[0]);
            Note.NoteName harmonyRoot = Note.NoteName.valueOf(parts[1]);
            Harmony.Tonality tonality = Harmony.Tonality.valueOf(parts[2]);
            int ranking = scores[Integer.parseInt(parts[3])];
            Harmony harmony = new Harmony(harmonyRoot, Harmony.Inversion.A, EnumSet.noneOf(Harmony.Dissonance.class), tonality);
            rankings.put(new NoteHarmonyMapping(melodyNote, harmony), ranking);
            System.out.println(melodyNote + " with " + harmonyRoot + " " + tonality + " scores " + ranking);
        } catch (Exception e) {
            System.out.println("Invalid syntax in chord rankings file -- " + fileLine);
        }

    }

    /**
     * Scores a harmony/note match -- the higher the match the better
     * Negative scores awarded for particularly bad matches
     * @param harmony -- harmony object to score
     * @param note -- note object to score
     * @return -- ranking of this pair
     */
    public static int getRanking(Harmony harmony, Note.NoteName note) {

        NoteHarmonyMapping mapping = new NoteHarmonyMapping(note, harmony);

        if (rankings.containsKey(mapping)) {
            return rankings.get(mapping);
        } else {
            return scores[0];
        }

    }

}
