package uk.ac.cam.ml692.bachnet.genetic;

import uk.ac.cam.ml692.bachnet.Harmony;
import uk.ac.cam.ml692.bachnet.Note;

/**
 * Mapping from melody note to harmony object
 * Used as a key into Chord Rankings
 */
public class NoteHarmonyMapping {

    /**
     * Mapping members
     */
    private Note.NoteName noteName;
    private Harmony harmony;

    /**
     * Constructor for this mapping
     * @param noteName - melody note
     * @param harmony - harmony object
     */
    public NoteHarmonyMapping(Note.NoteName noteName, Harmony harmony) {
        this.noteName = noteName;
        this.harmony = harmony;
    }

    /**
     * Get the NoteName
     * @return Note.NoteName
     */
    public Note.NoteName getNoteName() {
        return noteName;
    }

    /**
     * Get the Harmony
     * @return Harmony
     */
    public Harmony getHarmony() {
        return harmony;
    }

    /**
     * Equality test for HashMap
     * @param o -- object to compare with
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NoteHarmonyMapping)) return false;
        NoteHarmonyMapping mapping = (NoteHarmonyMapping) o;
        return mapping.getNoteName() == noteName && mapping.getHarmony().getRootNote() == harmony.getRootNote() && mapping.getHarmony().getTonality() == harmony.getTonality();
    }

    /**
     * Generate a hash code for this mapping for use in HashMap
     * @return
     */
    @Override
    public int hashCode() {
        return this.noteName.toString().hashCode()
                + this.harmony.getRootNote().toString().hashCode()
                + this.harmony.getTonality().toString().hashCode();
    }

}
