package uk.ac.cam.ml692.bachnet.genetic;

import uk.ac.cam.ml692.bachnet.Harmony;
import uk.ac.cam.ml692.bachnet.Note;
import uk.ac.cam.ml692.bachnet.Note.NoteName;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Handles import and maintenance of chord progression scores
 */

public class ChordProgressionRankings {

    /**
     * HashMap of rankings
     */
    private static HashMap<ChordProgression, Integer> rankings = new HashMap<>();

    /**
     * Load the rankings from a txt config file
     */
    public static void loadRankings() {
        URL url = ChordProgressionRankings.class.getClassLoader().getResource("files/chordProgressions.txt");
        if (url != null) {
            try (Stream<String> stream = Files.lines(Paths.get(url.getFile()), Charset.defaultCharset())) {
                stream.forEach(ChordProgressionRankings::addToRankings);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Helper function for loadRankings()
     * @param fileLine -- String representing one line of input file
     */
    private static void addToRankings(String fileLine) {
        String[] parts = fileLine.split("\\s+");
        if (Objects.equals(parts[0], "#")) {
            System.out.println("Skipping line -- " + fileLine);
            return;
        }
        if (parts.length != 5) {
            System.out.println("Invalid syntax in chord rankings file -- " + fileLine);
            return;
        }
        try {
            Note.NoteName firstHarmonyRoot = Note.NoteName.valueOf(parts[0]);
            Harmony.Tonality firstTonality = Harmony.Tonality.valueOf(parts[1]);
            Note.NoteName secondHarmonyRoot = Note.NoteName.valueOf(parts[2]);
            Harmony.Tonality secondTonality = Harmony.Tonality.valueOf(parts[3]);
            int percentage = Integer.parseInt(parts[4]);
            Harmony firstHarmony = new Harmony(firstHarmonyRoot, Harmony.Inversion.A, EnumSet.noneOf(Harmony.Dissonance.class), firstTonality);
            Harmony secondHarmony = new Harmony(secondHarmonyRoot, Harmony.Inversion.A, EnumSet.noneOf(Harmony.Dissonance.class), secondTonality);
            rankings.put(new ChordProgression(firstHarmony, secondHarmony), percentage);
            System.out.println(firstHarmonyRoot + " " + firstTonality + " then " + secondHarmonyRoot + " " + secondTonality + " occurs with percentage " + percentage + "%.");
        } catch (Exception e) {
            System.out.println("Invalid syntax in chord rankings file -- " + fileLine);
        }

    }

    /**
     * Scores a returns the score for a pair of chords (first, second)
     * 0 score awarded for bad/non-existent matches
     * @param firstChord -- harmony object to score
     * @param secondChord -- note object to score
     * @return -- ranking of this pair
     */
    public static int getRanking(Harmony firstChord, Harmony secondChord) {

        ChordProgression chordProgression = new ChordProgression(firstChord, secondChord);

        if (rankings.containsKey(chordProgression)) {
            return rankings.get(chordProgression);
        } else {
            return 0;
        }

    }

}
