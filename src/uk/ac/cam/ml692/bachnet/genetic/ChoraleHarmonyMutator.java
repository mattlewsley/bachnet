
package uk.ac.cam.ml692.bachnet.genetic;

import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import uk.ac.cam.ml692.bachnet.*;

import java.util.*;


/**
 * Chorale mutator for GA
 */

// TODO: Other mutation rules
// TODO: Introduce poission number generator for number of mutations? Then select random position to mutate
// TODO: Introduce number generator for mutation probability?
// TODO: Introduce mutationAmount aswell? Could be DiscreteUniformGenerator?

public class ChoraleHarmonyMutator implements EvolutionaryOperator<Chorale> {

    /**
     * Keep a list of mutation probabilities for each beat
     */
    private ArrayList<Probability> mutationProbabilities;

    /**
     * Construct mutator, instantiate mutationProbabilities
     * @param mutationProbability -- initial probability of mutation at each beat
     * @param lengthOfChorale -- length of chorale (for mutationProbabilities length)
     */
    public ChoraleHarmonyMutator(Probability mutationProbability, int lengthOfChorale) {
        mutationProbabilities = new ArrayList<>(lengthOfChorale);
        for (int i = 0; i <= lengthOfChorale; i++) {
            mutationProbabilities.add(i, mutationProbability);
        }
    }

    /**
     * Apply the mutation to a list of candidates
     * @param selectedCandidates -- candidates to mutate
     * @param random -- PRNG
     * @return -- List of mutated chorales
     */
    @Override
    public List<Chorale> apply(List<Chorale> selectedCandidates, Random random) {
        List<Chorale> mutatedPopulation = new ArrayList<>(selectedCandidates.size());
        for (Chorale c : selectedCandidates) {
            mutatedPopulation.add(mutate(c, random));
        }
        return mutatedPopulation;
    }

    /**
     * Mutate a given chorale
     * @param c -- chorale to mutate
     * @param random -- PRNG
     * @return -- mutated chorale
     */
    private Chorale mutate(Chorale c, Random random) {

        // Clone the pre-mutation chorale
        Chorale mutated = new Chorale();
        mutated.setHarmonies(c.getHarmonies());
        mutated.addSopranoLine(c.getSopranoLine());

        MelodicSequence sopranoLine = mutated.getSopranoLine();
        HarmonicSequence harmonicSequence = mutated.getHarmonies();

        for (Map.Entry<Integer, Harmony> entry : harmonicSequence.getHarmonies().entrySet()) {

            int beat = entry.getKey();

            double matchingScore = ChordRankings.getRanking(entry.getValue(), sopranoLine.getNote(beat).getFirst().getName());
            double progressionScore = 0;
            if (harmonicSequence.containsBeat(beat + 1)) {
                progressionScore += ChordProgressionRankings.getRanking(entry.getValue(), harmonicSequence.getHarmony(beat+1));
            }
            if (harmonicSequence.containsBeat(beat - 1)) {
                progressionScore += ChordProgressionRankings.getRanking(harmonicSequence.getHarmony(beat - 1), entry.getValue());
            }

            double totalScore = matchingScore + progressionScore;

            double scale = 1 - (Math.log(totalScore + 1) / 10000);

            if (matchingScore != 40) {

                if (random.nextDouble() < mutationProbabilities.get(beat).doubleValue()) {
                    entry.getValue().randomise(random);
                }

            }

        }

        return mutated;
    }

}
