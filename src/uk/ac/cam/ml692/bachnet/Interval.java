package uk.ac.cam.ml692.bachnet;

/**
 * Created by mattlewsley on 22/11/15.
 */
public class Interval {

    private Note.NoteName note1;
    private Note.NoteName note2;
    private int interval;

    public Interval(Note.NoteName n1, Note.NoteName n2) {
        note1 = n1;
        note2 = n2;
        int temp = n2.getValue() - n1.getValue();
        if (temp < 0) temp += 12;
        interval = temp;
    }

    public int getInterval() {
        return interval;
    }

    public Note.NoteName getNote1() { return note1; }
    public Note.NoteName getNote2() { return note2; }

    public boolean containsNote(Note.NoteName note) {
        return (note1 == note | note2 == note);
    }

}
