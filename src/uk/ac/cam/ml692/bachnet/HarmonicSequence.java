package uk.ac.cam.ml692.bachnet;

import javax.sound.midi.*;
import java.util.*;

public class HarmonicSequence implements Cloneable {

    // HashMap to store harmonies and their beat position
    private TreeMap<Integer, Harmony> harmonies;

    /**
     * Constructor to construct an empty HarmonicSequence
     */
    public HarmonicSequence() {
        harmonies = new TreeMap<>();
    }

    // TODO: Handle harmonies that span more than one note
    /**
     * Constructor to construct a HarmonicSequence from a MIDI sequence via interval analysis
     * @param chorale
     */
    public HarmonicSequence(Chorale chorale) {

        harmonies = new TreeMap<>();

        ArrayList<MelodicSequence> tracks = chorale.getTracks();

        int highestBeat = chorale.getSopranoLine().getHighestBeat();

        for (int beat = 0; beat <= highestBeat; beat++) {
            LinkedList<Note> notesToTest = new LinkedList<>();
            for (int track = 0; track < tracks.size(); track++) {
                if (tracks.get(track).hasNoteAtIndex(beat)) {
                    LinkedList<Note> notes = tracks.get(track).getNote(beat);
                    Note firstNote = notes.getFirst();
                    notesToTest.add(firstNote);
                }
            }
            try {
                Harmony harmony = new Harmony(notesToTest);
                if (tracks.get(3).hasNoteAtIndex(beat)) {
                    Note rootNote = tracks.get(3).getNote(beat).getFirst();
                    if (!rootNote.isRest()) {
                        if (rootNote.getName() != harmony.getRootNote()) {
                            Interval interval = new Interval(harmony.getRootNote(), rootNote.getName());
                            if (interval.getInterval() == 3 || interval.getInterval() == 4) {
                                harmony.setInversion(Harmony.Inversion.B);
                            }
                            if (interval.getInterval() == 7) {
                                harmony.setInversion(Harmony.Inversion.C);
                            }
                            if (interval.getInterval() == 10) {
                                harmony.setInversion(Harmony.Inversion.D);
                            }
                        }
                    }
                }
                harmonies.put(beat, harmony);
            } catch (UnableToDetermineHarmonyException e) {
                // Should print here about interval analysis failing but it gets annoying
            }
        }
    }

    /**
     * Add a harmony to the sequence, at position specified by index
     * @param index
     * @param h
     */
    public void addHarmony(int index, Harmony h) {
        harmonies.put(index, h);
    }

    /**
     * Return HashMap of all harmonies in the sequence
     * @return
     */
    public TreeMap<Integer, Harmony> getHarmonies() {
        return harmonies;
    }

    /**
     * Get the harmony at the specified index
     * @param index
     * @return
     */
    public Harmony getHarmony(int index) {
        return harmonies.get(index);
    }

    /**
     * Verify if the HarmonicSequence contains the beat specified by index
     * @param index - beat to check
     * @return true/false
     */
    public boolean containsBeat(int index) {
        return harmonies.get(index) != null;
    }

    /**
     * Get lowest key in harmonic sequence (this is the first beat we have a harmony for)
     * @return lowest beat
     */
    public int lowestBeat() {
        return harmonies.firstKey();
    }

    /**
     * Get highest key in harmonic sequence
     * @return highest beat
     */
    public int highestBeat() {
        return harmonies.lastKey();
    }

    /**
     * Clones a harmonic sequence, overcoming the problem with TreeMap.clone() only doing a shallow copy
     * Manually calls Harmony.clone() on each harmony object in the map and adds it to the output sequence
     * @return Cloned sequence
     */
    @SuppressWarnings("CloneDoesntCallSuperClone")
    @Override
    public HarmonicSequence clone() {
        HarmonicSequence harmonicSequence = new HarmonicSequence();
        for (Map.Entry<Integer, Harmony> entry : this.getHarmonies().entrySet()) {
            Harmony harmony = entry.getValue().clone();
            harmonicSequence.addHarmony(entry.getKey(), harmony);
        }
        return harmonicSequence;
    }

    /**
     * Print details of this sequence to console
     */
    public void printDetails() {
        for (Map.Entry<Integer, Harmony> entry : harmonies.entrySet()) {
            Harmony harmony = entry.getValue();
            System.out.println("Beat " + entry.getKey() + " : " + harmony.getRootNote() + " " + harmony.getTonality());
        }
    }

}
