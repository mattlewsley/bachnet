package uk.ac.cam.ml692.bachnet.neural;

import org.neuroph.core.Layer;
import org.neuroph.core.Neuron;
import org.neuroph.core.transfer.TransferFunction;

/**
 * Adapted from the org.neuroph.contrib.learning.SoftMax.java example to allow us to perform the SoftMax TransferFunction
 * over sections of our output individually
 */
public class BachNetSoftMax extends TransferFunction {

    // Transfer function state
    // neuronIndex tells us the index of the Neuron this TransferFunction is attached to within the 21 output bits
    // layer is a reference to the output layer, so that we can get all the neurons to iterate over
    private Layer layer;
    private int neuronIndex;

    /**
     * Constructor, initialises layer + neuronIndex
     * @param layer
     * @param index
     */
    public BachNetSoftMax(Layer layer, int index) {
        this.layer = layer;
        neuronIndex = index;
    }

    /**
     * Takes the netInput to this neuron as a parameter and returns the SoftMax output (per segment of output)
     * @param netInput
     * @return
     */
    @Override
    public double getOutput(double netInput) {

        // Avoid NaN outputs

        if (netInput > 50) {
            output = 1;
            return output;
        }

        if (netInput < -50) {
            output = 0;
            return output;
        }

        if (neuronIndex < 12) { // root note
            output = Math.exp(netInput) / total(0, 12);
        }

        else if (neuronIndex < 16) { // inversion
            output = Math.exp(netInput) / total(12, 16);
        }

        else if (neuronIndex < 18) { // tonality
            output = Math.exp(netInput) / total(16, 18);
        }

        else if (neuronIndex < 21) { // dissonance
            output = Math.exp(netInput) / total(18, 21);
        }

        return output;
    }

    /**
     * Helper function to calculate the sum of exp(input) for all the neurons on the output layer
     * @param min
     * @param max
     * @return
     */
    private double total(int min, int max) {

        Neuron[] neurons = layer.getNeurons();

        double currentTotal = 0;

        for (int i = min; i < max; i++) {
            // This is a bit hacky. Neuroph doesn't set the netInput field on the neurons to the current value until we're calculating for that particular neuron, so we have to force it to do that here before adding to the total
            // Eliminates bug where SoftMax totals were not summing to 1 (apart from the odd negligible floating point error)
            neurons[i].setInput(neurons[i].getInputFunction().getOutput(neurons[i].getInputConnections()));
            currentTotal += Math.exp(neurons[i].getNetInput());

        }

        return currentTotal;

    }

    /**
     * Function to get the derivative of the SoftMax output
     * @param net
     * @return
     */
    @Override
    public double getDerivative(double net) {
        return output * (1d - output);
    }

}
