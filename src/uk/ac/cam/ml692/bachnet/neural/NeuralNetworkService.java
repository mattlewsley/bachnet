
package uk.ac.cam.ml692.bachnet.neural;

import org.neuroph.contrib.learning.CrossEntropyError;
import org.neuroph.contrib.learning.SoftMax;
import org.neuroph.core.Layer;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.learning.LearningRule;
import org.neuroph.nnet.ConvolutionalNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.util.TransferFunctionType;
import uk.ac.cam.ml692.bachnet.*;
import uk.ac.cam.ml692.bachnet.genetic.ChordProgression;
import uk.ac.cam.ml692.bachnet.genetic.ChordRankings;
import uk.ac.cam.ml692.bachnet.neural.NeuralNetworkController;
import uk.ac.cam.ml692.bachnet.neural.NeuralNetworkHarmony;

import javax.sound.midi.*;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * Wrapper for the Neuroph Neural Network interactions
 * Should also make interop testing easier
 */

public class NeuralNetworkService {


    /**
     * Member variables
     */
    private NeuralNetwork neuralNetwork;
    private int windowSize;
    private String fullPath;
    private int inputBits;
    private int outputBits;
    private int hiddenBits;
    private static int maxIterations = 500;


    /**
     * Constructor -- loads net from disks or creates one if it doesn't already exist
     * @param window
     */
    public NeuralNetworkService(int window) {
        windowSize = window;
        inputBits = (window-1) * (NeuralNetworkHarmony.size) + (3*12);
        outputBits = NeuralNetworkHarmony.size;
        hiddenBits = (int) Math.floor(NeuralNetworkController.hiddenRatio * inputBits);
        fullPath = NeuralNetworkController.rootPath + windowSize + ".nnet";

        try { // Attempt to load from disk
            neuralNetwork = NeuralNetwork.load(fullPath);
        } catch (Exception e) { // Network doesn't already exist, so create one
            neuralNetwork = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, inputBits, hiddenBits, outputBits);
            neuralNetwork.save(fullPath);
        }
    }


    /**
     * Method to wrap saving the network back to disk
     */
    private void saveNetwork() {
        neuralNetwork.save(fullPath);
    }

    /**
     * Method to train the network from a MIDI file path (String)
     * It constructs a HarmonicSequence (mapping from integer representing beat to Harmony object) from the Sequence
     * It further constructs a melodicBeatSequence (mapping from integer representing beat to Note object) from the Sequence
     * melodicBeatSequence currently ignores any notes that don't occur on the beat (so we also handle rests/dotted notes incorrectly)
     * Training data is then constructed from the melodic at beat t, and the harmony at beat t-1, t-2, with the harmony at beat t as the output
     * Network is saved back to disk after training
     * @param midiPaths
     * @throws InvalidMidiDataException
     * @throws IOException
     */
    public void trainFromMidi(List<String> midiPaths) {

        ChordRankings.loadRankings();

        // Instantiate a Training Set
        DataSet trainingSet = new DataSet(inputBits, outputBits);

        // Meta variable
        int numberOfSamples = 0;

        // Iterate over strings
        for (String midiPath : midiPaths) {

            // Load the MIDI file as a sequence
            Sequence sequence;

            try {
                sequence = MidiSystem.getSequence(new File(midiPath));
            } catch (Exception e) {
                System.out.println("Error loading MIDI file " + midiPath + " -- skipping");
                continue;
            }

            if (sequence.getTracks().length > 5) {
                System.out.println("Error using MIDI Sequence " + midiPath + "  -- incorrect number of tracks");
                // Too many tracks (extra instrumentals) -- skip
                continue;
            }

            Chorale chorale = new Chorale(sequence);

            KeySignature keySignature = null;
            try {
                keySignature = chorale.getKeySignature();
            } catch (UnableToDetermineKeySignatureException e) {
                System.out.println("Error determining key signature for file " + midiPath + " -- skipping");
                continue;
            }

            if (keySignature.getTonality() != NeuralNetworkController.tonality) {
                System.out.println("Tonality of " + midiPath + " is not suitable for training with -- skipping");
                continue;
            }

            chorale.normalise();

            System.out.println("MIDI file " + midiPath + " successfully normalised, proceeding to train");

            HarmonicSequence harmonicSequence = chorale.getHarmonies();
            MelodicSequence melodyLine = chorale.getSopranoLine();

            for (int i = 0; i < melodyLine.getHighestBeat(); i++) {

                boolean canTrain = true;

                for (int j = 0; j < windowSize; j++) {
                    // Check all harmonies exist
                    canTrain = canTrain && harmonicSequence.containsBeat(i-j);
                }

                canTrain = canTrain && melodyLine.hasNoteAtIndex(i) && melodyLine.hasNoteAtIndex(i-1) && melodyLine.hasNoteAtIndex(i+1);

                System.out.println("Beat " + i);

                // TODO: Think about this some more. Currently not adding items to training set if they aren't considered good rankings
                // Helps to eliminate cases when interval analysis fails
                if (canTrain && ChordRankings.getRanking(harmonicSequence.getHarmony(i), melodyLine.getNote(i).getFirst().getName()) != 40) { canTrain = false; }

                if (!canTrain) continue;

                double[] melodyInput = melodyLine.getNote(i).getFirst().getName().toNeuralNetworkInput();
                double[] melodyInputBefore = melodyLine.getNote(i-1).getFirst().getName().toNeuralNetworkInput();
                double[] melodyInputAfter = melodyLine.getNote(i+1).getFirst().getName().toNeuralNetworkInput();

                double[] neuralNetworkInput = new double[inputBits];

                int inputIndex = 0;

                for (int windowNumber = (windowSize - 1); windowNumber > 0; windowNumber--) {

                    double[] harmonyInput = new NeuralNetworkHarmony(harmonicSequence.getHarmony(i-windowNumber)).toNeuralNetworkInput();

                    for (int j = 0; j < harmonyInput.length; j++) {
                        neuralNetworkInput[inputIndex] = harmonyInput[j];
                        inputIndex++;
                    }

                }

                for (int l = 0; l < melodyInput.length; l++) {
                    neuralNetworkInput[inputIndex] = melodyInput[l];
                    inputIndex++;
                }

                for (int l = 0; l < melodyInputBefore.length; l++) {
                    neuralNetworkInput[inputIndex] = melodyInputBefore[l];
                    inputIndex++;
                }

                for (int l = 0; l < melodyInputAfter.length; l++) {
                    neuralNetworkInput[inputIndex] = melodyInputAfter[l];
                    inputIndex++;
                }

                double[] neuralNetworkOutput = new NeuralNetworkHarmony(harmonicSequence.getHarmony(i)).toNeuralNetworkInput();

                System.out.println("Training on beat " + i + ": "
                        + harmonicSequence.getHarmony(i).getRootNote() + " " + harmonicSequence.getHarmony(i).getTonality() + " " + harmonicSequence.getHarmony(i).getInversion()
                        + ", Melody: " + melodyLine.getNote(i).getFirst().getName());

                numberOfSamples++;

                trainingSet.addRow(new DataSetRow(neuralNetworkInput, neuralNetworkOutput));

            }

        }

        // Create a back propagation learning rule

        BackPropagation backPropagation = new BackPropagation();
        backPropagation.setNeuralNetwork(neuralNetwork);

        // Set error function to be cross entropy

        backPropagation.setErrorFunction(new CrossEntropy());
        backPropagation.setLearningRate(0.05);

        // Override output layer to be BachNetSoftMax

        Layer outputLayer = neuralNetwork.getLayerAt(2);

        for (int i = 0; i < neuralNetwork.getOutputNeurons().length; i++) {
            BachNetSoftMax tf = new BachNetSoftMax(outputLayer, i);
            neuralNetwork.getOutputNeurons()[i].setTransferFunction(tf);
        }

        for (int x = 0; x < maxIterations; x++) {
            backPropagation.doOneLearningIteration(trainingSet);
            System.out.println("Network " + windowSize + " Iteration " + x + " Error " + backPropagation.getTotalNetworkError());
        }

        // Save to disk

        saveNetwork();

        System.out.println("Trained on a total of " + numberOfSamples + " samples.");

    }

    /**
     * Returns a map from Beat to double[] (neural network output) when given a MIDI Sequence
     * @param inputChorale
     * @throws InvalidMidiDataException
     * @throws IOException
     */
    public Map<Integer, Double[]> calculateFromMidi(Chorale inputChorale) {

        KeySignature keySignature = null;
        try {
            keySignature = inputChorale.getKeySignature();
        } catch (UnableToDetermineKeySignatureException e) {
            System.out.println("Unable to determine harmony -- normalisation will thus fail so expect bad results");
        }
        if (keySignature.getTonality() != NeuralNetworkController.tonality) {
            System.out.println("Warning: MIDI sequence suspected to be of the wrong tonality -- will calculate anyway but expect spurious results");
        }
        inputChorale.normalise();
        HarmonicSequence inputHarmonies = inputChorale.getHarmonies();

        // Instantiate an empty collection to store output harmonies
        Map<Integer, Double[]> outputs = new TreeMap<>();

        // TODO: Calculate/suggest (EG root -> fifth) first two harmonies instead of preserving
        // Collect first (windowSize-1) harmonies from inputHarmonies (to serve as ht-2 and ht-1 for the first calculation only)
        int smallestBeat = inputHarmonies.lowestBeat();

        for (int i = smallestBeat; i < (smallestBeat+windowSize-1); i++) {

            NeuralNetworkHarmony harmonyToAdd;

            // Just incase chord at beat 0, 1, 2 etc hasn't been recognised
            if (inputHarmonies.containsBeat(i)) {
                harmonyToAdd = new NeuralNetworkHarmony(inputHarmonies.getHarmony(i));
            } else {
                harmonyToAdd = new NeuralNetworkHarmony(new Harmony(Note.NoteName.C, Harmony.Inversion.A, EnumSet.noneOf(Harmony.Dissonance.class), Harmony.Tonality.MAJOR));
            }

            double[] doubleArrayPrim = harmonyToAdd.toNeuralNetworkInput();
            Double[] doubleArrayObj = NeuralNetworkController.doubleArrayPrimToObj(doubleArrayPrim);

            outputs.put(i, doubleArrayObj);
        }

        MelodicSequence melodyLine = inputChorale.getSopranoLine();

        // Calculate the outputs

        for (Map.Entry<Integer, LinkedList<Note>> entry : melodyLine.getNotes().entrySet()) {

            int currentBeat = entry.getKey();

            boolean canCompute = true;

            for (int j = 1; j < windowSize; j++) {
                canCompute = canCompute && outputs.containsKey(currentBeat - j);
            }

            canCompute = canCompute && melodyLine.hasNoteAtIndex(currentBeat-1) && melodyLine.hasNoteAtIndex(currentBeat+1);

            if (!canCompute) continue;

            // Instantiate empty double array for input
            double[] neuralNetworkInput = new double[inputBits];

            // Index into neuralNetworkInput
            int inputIndex = 0;

            for (int i = windowSize-1; i > 0; i--) {

                Double[] currentHarmony = outputs.get(currentBeat - i);

                // Add harmony object to neuralNetworkInput
                for (int j = 0; j<currentHarmony.length; j++) {
                    neuralNetworkInput[inputIndex] = currentHarmony[j];
                    inputIndex++;
                }

            }

            double[] melodyInput = entry.getValue().getFirst().getName().toNeuralNetworkInput();
            double[] melodyInputBefore = melodyLine.getNote(currentBeat-1).getFirst().getName().toNeuralNetworkInput();
            double[] melodyInputAfter = melodyLine.getNote(currentBeat + 1).getFirst().getName().toNeuralNetworkInput();

            // Add melody to neuralNetworkInput
            for (int l = 0; l<melodyInput.length; l++) {
                neuralNetworkInput[inputIndex] = melodyInput[l];
                inputIndex++;
            }

            for (int l = 0; l<melodyInputBefore.length; l++) {
                neuralNetworkInput[inputIndex] = melodyInputBefore[l];
                inputIndex++;
            }

            for (int l = 0; l<melodyInputAfter.length; l++) {
                neuralNetworkInput[inputIndex] = melodyInputAfter[l];
                inputIndex++;
            }

            // Calculate output
            neuralNetwork.setInput(neuralNetworkInput);
            neuralNetwork.calculate();

            // Get output
            double[] networkOutput = neuralNetwork.getOutput();

            Double[] networkOutputObj = NeuralNetworkController.doubleArrayPrimToObj(networkOutput);

            // Convert to a Harmony object
            Harmony outputHarmony = new Harmony(networkOutput);

            // Add that harmony object to the output harmonies
            outputs.put(currentBeat, networkOutputObj);

//            System.out.println("Calculated beat " + currentBeat + " with " + outputHarmony.getRootNote() + " " + outputHarmony.getTonality() + ", melody note " + entry.getValue().getFirst().getName());

        }

        return outputs;

    }

}