package uk.ac.cam.ml692.bachnet.neural;

import uk.ac.cam.ml692.bachnet.Harmony;
import uk.ac.cam.ml692.bachnet.Note;

import java.util.BitSet;

/**
 * Created by mattlewsley on 18/11/15.
 */
public class NeuralNetworkHarmony {

    // BitSet objects to store root, tonality, inversion and dissonances
    private BitSet rootNote;
    private BitSet tonality;
    private BitSet inversion;
    private BitSet dissonances;
    public static int size = 21;

    /**
     * Constructor
     * Initialises BitSets to size of the relevant enumerations
     */
    public NeuralNetworkHarmony() {
        rootNote = new BitSet(Note.NoteName.values().length-1); // Encode rests as all 0s
        inversion = new BitSet(Harmony.Inversion.values().length);
        dissonances = new BitSet(Harmony.Dissonance.values().length);
        tonality = new BitSet(Harmony.Tonality.values().length);
    }

    /**
     * Constructor
     * Constructs a NeuralNetworkHarmony object from a Harmony object
     * @param harmony
     */
    public NeuralNetworkHarmony(Harmony harmony) {
        rootNote = new BitSet(Note.NoteName.values().length-1);
        inversion = new BitSet(Harmony.Inversion.values().length);
        dissonances = new BitSet(Harmony.Dissonance.values().length);
        tonality = new BitSet(Harmony.Tonality.values().length);
        rootNote.set(harmony.getRootNote().ordinal());
        inversion.set(harmony.getInversion().ordinal());
        tonality.set(harmony.getTonality().ordinal());

        for (Harmony.Dissonance d : harmony.getDissonances()) {
            dissonances.set(d.ordinal());
        }
    }

    /**
     * Converts to a double array for direct input to the NeuralNetwork
     * @return
     */
    public double[] toNeuralNetworkInput() {

        int arraySize = Note.NoteName.values().length-1 + Harmony.Inversion.values().length + Harmony.Dissonance.values().length + Harmony.Tonality.values().length;

        double[] output = new double[arraySize];

        int arrayIndex = 0;

        // Handle root note
        for (int i  = rootNote.nextSetBit(0); i >= 0; i = rootNote.nextSetBit(i+1)) {
            output[i + arrayIndex] = 1;
        }

        arrayIndex += Note.NoteName.values().length-1;

        // Handle inversion
        for (int i  = inversion.nextSetBit(0); i >= 0; i = inversion.nextSetBit(i+1)) {
            output[i + arrayIndex] = 1;
        }

        arrayIndex += Harmony.Inversion.values().length;

        // Handle tonality
        for (int i  = tonality.nextSetBit(0); i >= 0; i = tonality.nextSetBit(i+1)) {
            output[i + arrayIndex] = 1;
        }

        arrayIndex += Harmony.Tonality.values().length;

        // Handle dissonances
        for (int i  = dissonances.nextSetBit(0); i >= 0; i = dissonances.nextSetBit(i+1)) {
            output[i + arrayIndex] = 1;
        }

        return output;
    }

}
