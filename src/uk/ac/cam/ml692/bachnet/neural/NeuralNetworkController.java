package uk.ac.cam.ml692.bachnet.neural;

import org.neuroph.util.NeurophArrayList;
import uk.ac.cam.ml692.bachnet.*;
import uk.ac.cam.ml692.bachnet.genetic.ChoraleHarmonyFitnessEvaluator;
import uk.ac.cam.ml692.bachnet.genetic.GeneticController;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Class to help arbitrate between various Neural Networks of different window sizes
 */
public class NeuralNetworkController {

    // Path where .nnet files will be saved/can be located
    public static String rootPath = "/Users/mattlewsley/Documents/Bachnet/";
    // Folder with training data in
    public static String trainingPath = "TrainingData/";
    // Rate at which to apply passing notes
    private static double passingNoteRate = 0.5;
    // Array of window sizes to use
    private static int[] windowSizes = {3,5,7};
    // Ratio of hidden neurons to input neurons (will be floored)
    public static double hiddenRatio = 0.95;
    // Maximum number of files to train from in a folder
    private static int trainingLimit = 200;
    // Tonality of this NeuNet Config
    public static Harmony.Tonality tonality = Harmony.Tonality.MAJOR;

    /**
     * Helper function to help us convert from Double[] to double[]
     *
     * @param input
     * @return
     */
    public static double[] doubleArrayObjToPrim(Double[] input) {
        double[] output = new double[input.length];
        for (int x = 0; x < input.length; x++) {
            output[x] = input[x];
        }
        return output;
    }

    /**
     * Helper function to help us convert from double[] to Double[]
     *
     * @param input
     * @return
     */
    public static Double[] doubleArrayPrimToObj(double[] input) {
        Double[] output = new Double[input.length];
        for (int x = 0; x < input.length; x++) {
            output[x] = input[x];
        }
        return output;
    }

    /**
     * Get a collection of all NeuralNetworkServices according to windowSizes specified
     * @return
     */
    public static ArrayList<NeuralNetworkService> getNetworks() {
        ArrayList<NeuralNetworkService> networks = new ArrayList<>();
        for (int i : windowSizes) {
            networks.add(new NeuralNetworkService(i));
        }
        return networks;
    }

    /**
     * Train all the networks with the provided array of midi file paths
     *
     * @param midiPaths -- array of strings containing midi file paths
     */
    public static void trainNetworks(List<String> midiPaths) {

        // Iterate over networks
        for (NeuralNetworkService network : getNetworks()) {
            network.trainFromMidi(midiPaths);
        }

        System.out.println("Trained " + windowSizes.length + " networks on " + midiPaths.size() + " MIDI files");

    }

    // TODO: Choose probabilistically
    /**
     * Calculates the results from all networks for a given midi file path and aggregates the outputs
     *
     * @param midiPath -- input file to be calculated
     * @return
     * @throws InvalidMidiDataException
     * @throws IOException
     */
    public static HarmonicSequence calculateFromMidi(String midiPath) throws InvalidMidiDataException, IOException {

        HarmonicSequence outputHarmonies = new HarmonicSequence();

        Sequence sequence = MidiSystem.getSequence(new File(midiPath));

        Chorale chorale = new Chorale(sequence);

        ArrayList<NeuralNetworkService> networks = getNetworks();

        ArrayList<Map<Integer, Double[]>> networkOutputs = new ArrayList<>();

        // Iterate over networks
        for (NeuralNetworkService network : networks) {
            networkOutputs.add(network.calculateFromMidi(chorale));
        }

        for (int currentBeat = 0; currentBeat < chorale.getSopranoLine().getHighestBeat(); currentBeat++) {

            LinkedList<Double[]> outputsToAverage = new LinkedList<>();

            for (Map<Integer, Double[]> output : networkOutputs) {
                if (output.containsKey(currentBeat)) {
                    outputsToAverage.add(output.get(currentBeat));
                    Harmony suggestion = new Harmony(doubleArrayObjToPrim(output.get(currentBeat)));
                    System.out.println("Beat " + currentBeat + "Network suggests " + suggestion.getRootNote() + " " + suggestion.getTonality() + " " + suggestion.getInversion());
                    System.out.println(Arrays.toString(output.get(currentBeat)));
                }
            }

            double[] finalOutput = new double[21];

            // Add all network suggestions together
            for (Double[] outputToAdd : outputsToAverage) {
                for (int i = 0; i < outputToAdd.length; i++) {
                    finalOutput[i] += outputToAdd[i];
                }
            }

            // TODO: Devise a better selection strategy -- EG voting rather than averaging

            // Divide by total number of network suggestions
            for (int i = 0; i < finalOutput.length; i++) {
                finalOutput[i] = finalOutput[i] / (outputsToAverage.size());
            }

            // Get Harmony object from aggregate double array
            Harmony currentHarmony = new Harmony(finalOutput);

            outputHarmonies.addHarmony(currentBeat, currentHarmony);

        }

        return outputHarmonies;

    }

    /**
     * Train networks using all files in the specified folder path
     * @param folderPath
     */
    public static void trainFromFolder(String folderPath) {
        File inputMIDIFolder = new File(folderPath);

        File[] inputMIDIFiles = inputMIDIFolder.listFiles();
        List<String> inputMIDIFilePaths = new LinkedList<>();

        int numberOfTrainingFiles = 0;

        for (File file : inputMIDIFiles) {
            if (file.isFile() && !file.isHidden() && numberOfTrainingFiles < trainingLimit) {
                inputMIDIFilePaths.add(file.getPath());
                numberOfTrainingFiles++;
            }
        }

        Collections.shuffle(inputMIDIFilePaths);

        trainNetworks(inputMIDIFilePaths);

    }

    /**
     * Using this as an entry point for the NeuralNetwork application
     * @param args
     * @throws InvalidMidiDataException
     * @throws IOException
     */
    public static void main(String[] args) throws InvalidMidiDataException, IOException {

        trainFromFolder("/Users/mattlewsley/Documents/BachNet/TrainingData/");

        File inputMIDIFolder = new File("/Users/mattlewsley/Documents/BachNet/TrainingData/");

        File[] inputMIDIFiles = inputMIDIFolder.listFiles();
        List<String> inputMIDIFilePaths = new LinkedList<>();

        int numberOfTrainingFiles = 0;

        for (File file : inputMIDIFiles) {
            if (file.isFile() && !file.isHidden() && numberOfTrainingFiles < trainingLimit) {
                inputMIDIFilePaths.add(file.getPath());
                numberOfTrainingFiles++;
            }
        }
//
//        // TODO: Simplify this process -- calculateFromMidi should maybe take a string and return a Chorale, wrapping these steps
//
        String bestFile = "";
        int bestScore = -10000;

        for (String file : inputMIDIFilePaths) {
            try {
                File theFile = new File(file);
                HarmonicSequence harmonicSequence = calculateFromMidi(file);
                Sequence sequence = MidiSystem.getSequence(theFile);
                Chorale chorale = new Chorale(sequence);
                chorale.normalise();
                chorale.setHarmonies(harmonicSequence);
                int score = GeneticController.analyseChorale(chorale);
                if (score > bestScore) { bestScore = score; bestFile = file; }
                chorale.harmonicSequenceToMelodicSequences();
                chorale.addPassingNotes(0.75);
//                MIDI.writeChoraleToMIDI(chorale, "/Users/mattlewsley/Documents/BachNet/Temp/" + theFile.getName());
            } catch (Exception e) {
                System.out.println("Skipping " + file);
            }
        }

        System.out.println(bestFile + " " + bestScore);

    }

}


