package uk.ac.cam.ml692.bachnet.neural;

import org.neuroph.core.learning.error.ErrorFunction;

import java.io.Serializable;

/**
 * Created by mattlewsley on 27/01/16.
 */
public class CrossEntropy implements ErrorFunction, Serializable {

    /*private transient double totalError;

    *//**
     * Number of patterns - n
     *//*
    private transient double patternCount;

    public CrossEntropy() {
        reset();
    }

    @Override
    public void reset() {
        totalError = 0d;
        patternCount = 0;
    }


    @Override
    public double getTotalError() {
        return -1d * totalError / ( patternCount );
    }

    @Override
    public double[]calculatePatternError(double[] predictedOutput, double[] targetOutput) {
        double[] patternError = new double[targetOutput.length];

        for (int i = 0; i < predictedOutput.length; i++) {
            patternError[i] =  Math.log(predictedOutput[i]) * targetOutput[i];
            totalError += patternError[i];
        }

        patternCount++;
        return patternError;
    }*/

    private double[] patternError;
    private transient double totalError;
    private transient double patternCount;

    @Override
    public double getTotalError() {
        return -totalError / patternCount ;
    }

    @Override
    public void reset() {
        totalError = 0d;
        patternCount = 0;
    }

    @Override
    public double[] calculatePatternError(double[] predictedOutput, double[] targetOutput) {

        patternError = new double[targetOutput.length];

        if (predictedOutput.length != targetOutput.length)
            throw new IllegalArgumentException("Output array length and desired output array length must be the same size!");

        int totalErrorIndex = 0;

        // TODO: Separate weightings

        for (int i = 0; i < predictedOutput.length; i++) {

            patternError[i] =  targetOutput[i] - predictedOutput[i];
//            double tempLogPredictedOutput = Math.log(predictedOutput[i]);
//            double tempLogPredictedOutput2 = Math.log(1-predictedOutput[i]);
//            if (Double.isNaN(tempLogPredictedOutput) || Double.isInfinite(tempLogPredictedOutput)) tempLogPredictedOutput = -1;
//            if (Double.isNaN(tempLogPredictedOutput2) || Double.isInfinite(tempLogPredictedOutput2)) tempLogPredictedOutput2 = -1;

            double tempLogPredictedOutput = (predictedOutput[i] < 0.0001) ? -2 : Math.log(predictedOutput[i]);
            double tempLogPredictedOutput2 = ((1-predictedOutput[i]) < 0.0001) ? -2 : Math.log(1-predictedOutput[i]);

            totalError += targetOutput[i] * tempLogPredictedOutput + (1-targetOutput[i]) * tempLogPredictedOutput2;

        }

        patternCount++;

        return patternError;
    }

}
