package uk.ac.cam.ml692.bachnet;

/**
 * Created by mattlewsley on 08/04/16.
 */
public class UnableToDetermineHarmonyException extends Throwable {

    private String message;

    public UnableToDetermineHarmonyException(String s) {
        message = s;
    }

    public String getMessage() {
        return message;
    }

}
