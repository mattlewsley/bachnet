package uk.ac.cam.ml692.bachnet;

import javax.sound.midi.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;

public class MIDI {

    // MIDI Output Settings
    public static final float outputDivisionType = 0.0f;
    public static final int outputResolution = 1024;
    public static final int outputNumTracks = 4;

    // MIDI Status Bytes
    public static final int NOTE_ON = 0x90;
    public static final int NOTE_OFF = 0x80;

    // MIDI Meta Message Types
    public static final int KEY_SIG = 0x59;
    public static final int TIME_SIG = 0x58;
    public static final int TEMPO = 0x51;
    public static final int END_OF_TRACK = 0x2f;

    /**
     * Writes a Chorale out to a MIDI file at the specified path
     * @param chorale -- Chorale to write
     * @param filePath -- file path to write to
     * @throws IOException
     * @throws InvalidMidiDataException
     */
    public static void writeChoraleToMIDI(Chorale chorale, String filePath) throws IOException, InvalidMidiDataException {

        Sequence outputSequence = annotateWithChordSymbols(chorale);
        MidiSystem.write(outputSequence, 1, new File(filePath));

    }

    /**
     * Takes a Chorale and annotates it with chord symbols, returning the MIDI sequence
     * @param chorale -- Chorale which we want to turn into a sequence and annotate
     * @throws InvalidMidiDataException
     * @throws IOException
     */
    public static Sequence annotateWithChordSymbols(Chorale chorale) throws InvalidMidiDataException, IOException {

        Sequence outputSequence = chorale.toSequence();
        HarmonicSequence harmonies = chorale.getHarmonies();

        // Let's annotate the soprano line at index 1
        Track trackToAnnotate = outputSequence.getTracks()[1];

        int resolution = outputSequence.getResolution();

        // Is this the best way of getting the max beats? Length of harmony collection might be better?
        int numberOfBeats = chorale.getSopranoLine().getHighestBeat();

        // TODO: Handle dissonance and inversion
        for (int i = 0; i <= numberOfBeats; i++) {
            if (harmonies.containsBeat(i)) {

                Harmony harmony = harmonies.getHarmony(i);

                String tonality = (harmony.getTonality().toString() == "MAJOR") ? "" : "m";
                String messageText = harmony.getRootNote() + " " + tonality;

                byte[] messageTextByteArray = messageText.getBytes();

                MetaMessage message = new MetaMessage(5, messageTextByteArray, messageTextByteArray.length);
                MidiEvent myEvent = new MidiEvent(message, (long) (i * resolution));

                trackToAnnotate.add(myEvent);

            }
        }

        return outputSequence;

    }

    /**
     * Prints out MIDI file data in CSV format
     * Used to determine which files are best to train on
     * @throws InvalidMidiDataException
     * @throws IOException
     */
    public static void analyseTrainingData() throws InvalidMidiDataException, IOException {
        File inputMIDIFolder = new File("/Users/mattlewsley/Documents/BachNet/TrainingData/");
        File[] inputMIDIFiles = inputMIDIFolder.listFiles();

        ArrayList<String> inputMIDIFilePaths = new ArrayList<>();
        for (File file : inputMIDIFiles) {
            if (file.isFile() && !file.isHidden()) {
                inputMIDIFilePaths.add(file.getPath());
            }
        }

        for (String filePath : inputMIDIFilePaths) {

            File file = new File(filePath);

            Sequence sequence = MidiSystem.getSequence(file);

            if (sequence.getTracks().length > 5) {
//                System.out.println("Input sequence has " + sequence.getTracks().length + " tracks -- skipping");
                continue;
            }

            Chorale chorale = new Chorale(sequence);

            KeySignature keySignature;

            try {
                keySignature = chorale.getKeySignature();
                System.out.print(file.getName());
                System.out.print(" " + keySignature.getRootNote() + " " + keySignature.getTonality());
            } catch (UnableToDetermineKeySignatureException e) {
//                System.out.println("Can't determine KeySignature -- skipping");
                continue;
            }

            System.out.print(" " + chorale.getSopranoLine().getHighestBeat());
            System.out.print(" " + chorale.getHarmonies().getHarmonies().size() + System.lineSeparator());


        }
    }


}