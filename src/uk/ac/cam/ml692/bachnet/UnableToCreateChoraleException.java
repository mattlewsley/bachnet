package uk.ac.cam.ml692.bachnet;

/**
 * Created by mattlewsley on 24/04/16.
 */
public class UnableToCreateChoraleException extends Throwable {

    private String message;

    public UnableToCreateChoraleException(String s) {
        message = s;
    }

    public String getMessage() {
        return message;
    }

}
