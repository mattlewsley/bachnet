package uk.ac.cam.ml692.bachnet;

/**
 * Exception thrown by the calculateKeySignature() method in MIDI when it cannot (naively) determine the key sig
 */
public class UnableToDetermineKeySignatureException extends Throwable {

    private String message;

    public UnableToDetermineKeySignatureException(String s) {
        message = s;
    }

    public String getMessage() {
        return message;
    }

}
