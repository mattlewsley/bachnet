package uk.ac.cam.ml692.bachnet;

import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Track;

/**
 * Created by mattlewsley on 01/04/16.
 */
public class TimeSignature {

    private int top;
    private int bottom;

    public TimeSignature(int top, int bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    public TimeSignature(Track track) {

        for (int i = 0; i < track.size(); i++) {
            MidiEvent event = track.get(i);
            MidiMessage message = event.getMessage();
            if (message instanceof MetaMessage) {
                MetaMessage metaMessage = (MetaMessage) message;
                if (metaMessage.getType() == MIDI.TIME_SIG) {
                    byte[] data = metaMessage.getData();
                    this.top = data[4];
                    this.bottom = (int) Math.pow(2, data[5]);
                    return;
                }
            }
        }

        // If no meta message found, just assume 4/4

        top = 4;
        bottom = 4;

    }

    public int getTop() {
        return top;
    }

    public int getBottom() {
        return bottom;
    }

}
