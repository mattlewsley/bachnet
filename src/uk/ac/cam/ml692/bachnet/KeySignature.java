package uk.ac.cam.ml692.bachnet;

import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Track;

/**
 * Created by mattlewsley on 01/04/16.
 */
public class KeySignature {

    private Note.NoteName rootNote;
    private Harmony.Tonality tonality;

    /**
     * Array to index when converting MIDI tonality encoding to a root note
     */
    private static final String[] roots = {"B", "FSharp", "CSharp", "GSharp", "DSharp", "ASharp", "F", "C", "G", "D", "A", "E", "B", "FSharp", "CSharp"};

    // TODO: Properly handle when we can't determine key signature
    // TODO: Handle anacruses
    /**
     * Main constructor for KeySignature
     * Takes a Chorale and attempts to extract KeySig first from MIDI info, and then from harmonic sequence
     * @param chorale
     * @throws UnableToDetermineKeySignatureException
     */
    public KeySignature(Chorale chorale) throws UnableToDetermineKeySignatureException {
        if (chorale.getOriginalMidiMetaTrack() != null) {
            try {
                fromMidiTrack(chorale.getOriginalMidiMetaTrack());
            } catch (UnableToDetermineKeySignatureException e) {
                try {
                    fromHarmonicSequence(chorale.getHarmonies());
                } catch (UnableToDetermineKeySignatureException e2) {
                    throw new UnableToDetermineKeySignatureException("Couldn't use meta information or HarmonicSequence to determine KeySignature");
                }
            }
        }
    }

    /**
     * Attempts to construct a KeySignature from a MIDI Track containing MetaMessage objects
     * Throws an UnableToDetermineKeySignatureException if it fails (can't find a key sig message in the track)
     * @param midiTrack -- track expected to contain key signature meta events
     * @throws UnableToDetermineKeySignatureException -- if track contains no such events
     */
    private void fromMidiTrack(Track midiTrack) throws UnableToDetermineKeySignatureException {

        for (int i = 0; i < midiTrack.size(); i++) {
            MidiEvent event = midiTrack.get(i);
            MidiMessage message = event.getMessage();

            if (message instanceof MetaMessage) {
                MetaMessage metaMessage = (MetaMessage) message;

                if (metaMessage.getType() == MIDI.KEY_SIG) {
                    byte[] messageData = metaMessage.getData();
                    String rootNoteString = roots[messageData[0] + 7];
                    this.rootNote = Note.NoteName.valueOf(rootNoteString);
                    this.tonality = (messageData[1] == 1) ? Harmony.Tonality.MINOR : Harmony.Tonality.MAJOR;
                    if (tonality == Harmony.Tonality.MINOR) { // Move down relative third
                        int newRootValue = rootNote.getValue() - 3;
                        if (newRootValue < 0) newRootValue += 12;
                        this.rootNote = Note.NoteName.get(newRootValue);
                    }
                    return;
                }
            }
        }

        throw new UnableToDetermineKeySignatureException("No key signature MetaMessage found on this MIDI track");

    }

    /**
     * Attempt to construct a KeySignature from a HarmonicSequence by comparing the first and last chord in the sequence
     * @param harmonicSequence
     * @throws UnableToDetermineKeySignatureException -- if first and last chord don't match
     */
    private void fromHarmonicSequence(HarmonicSequence harmonicSequence) throws UnableToDetermineKeySignatureException {

        // TODO: Account for time signature (1st chord as 1st chord on beat 1 of a bar -- adjust lowest key)
        // TODO: Handle tierce de picardie etc more explicitly
        // TODO: Handle when piece ends in relative major

        int lowestKey = 0;
        int highestKey = harmonicSequence.highestBeat();

        Harmony firstChord;
        Harmony lastChord;

        firstChord = harmonicSequence.getHarmony(lowestKey);
        lastChord = harmonicSequence.getHarmony(highestKey);

        if (firstChord == null || lastChord == null) {
            throw new UnableToDetermineKeySignatureException("Unable to determine first chord");
        }

        if (lastChord == null) {
            throw new UnableToDetermineKeySignatureException("Unable to determine last chord");
        }

        if (firstChord.getRootNote() == lastChord.getRootNote()) {
            this.rootNote = firstChord.getRootNote();
            this.tonality = firstChord.getTonality();
        } else {
            throw new UnableToDetermineKeySignatureException("Chords don't match: " + firstChord.getRootNote() + firstChord.getTonality() + " > " + lastChord.getRootNote() + lastChord.getTonality());
        }

    }

    public Note.NoteName getRootNote() {
        return rootNote;
    }

    public Harmony.Tonality getTonality() {
        return tonality;
    }

}
