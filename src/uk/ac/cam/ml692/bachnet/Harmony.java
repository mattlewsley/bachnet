package uk.ac.cam.ml692.bachnet;

import java.util.*;

/**
 * Created by mattlewsley on 18/11/15.
 */
public class Harmony implements Cloneable {

    /**
     * Inversion enumeration
     */
    public enum Inversion {
        A, B, C, D
    }

    // TODO: Consider how to represent a diminished chord (maybe should be another tonality since it's neither major nor minor)
    // Also need to think about whether it's a major or minor added note
    /**
     * Dissonance enumeration
     */
    public enum Dissonance {
        B5, ADDED7, ADDED9
    }

    /**
     * Tonality enumeration
     */
    public enum Tonality {
        MAJOR, MINOR
    }

    /**
     * Fields
     */
    private Note.NoteName rootNote;
    private Inversion inversion;
    private Tonality tonality;
    private EnumSet<Dissonance> dissonances;

    /**
     * Constructor for a Harmony object
     * @param root - root note
     * @param inv - inversion
     * @param diss - dissonances
     * @param t - tonality
     */
    public Harmony(Note.NoteName root, Inversion inv, EnumSet<Dissonance> diss, Tonality t) {
        rootNote = root;
        inversion = inv;
        dissonances = diss;
        tonality = t;
    }

    /**
     * Construct a Harmony object from a Neural Network output array of doubles
     * @param neuralOutput - double array representing neural network output
     */
    public Harmony(double[] neuralOutput) {
        int arrayIndex = 0;

        int noteIndex = 0;
        int inversionIndex = 0;
        int tonalityIndex = 0;
        int dissonanceIndex = 0;

        double maxNote = 0;
        double maxInversion = 0;
        double maxTonality = 0;
        double maxDissonance = 0;

        // TODO: Why did I even use a while loop here? Use for..

        while (arrayIndex < 12) {
            if (neuralOutput[arrayIndex] > maxNote) {
                noteIndex = arrayIndex;
                maxNote = neuralOutput[arrayIndex];
            }
            arrayIndex++;
        }

        while (arrayIndex < 16) {
            if (neuralOutput[arrayIndex] > maxInversion) {
                inversionIndex = arrayIndex - 12;
                maxInversion = neuralOutput[arrayIndex];
            }
            arrayIndex++;
        }

        while (arrayIndex< 18) {
            if (neuralOutput[arrayIndex] > maxTonality) {
                tonalityIndex = arrayIndex - 16;
                maxTonality = neuralOutput[arrayIndex];
            }
            arrayIndex++;
        }

        while (arrayIndex < 21) {
            if (neuralOutput[arrayIndex] > maxDissonance) {
                dissonanceIndex = arrayIndex - 18;
                maxDissonance = neuralOutput[arrayIndex];
            }
            arrayIndex++;
        }

        Note.NoteName noteName = Note.NoteName.values()[noteIndex];
        Inversion inversion = Inversion.values()[inversionIndex];
        Tonality tonality = Tonality.values()[tonalityIndex];

        // TODO: Handle dissonances

        rootNote = noteName;
        this.inversion = inversion;
        dissonances = EnumSet.noneOf(Dissonance.class);
        this.tonality = tonality;

    }

    /**
     * Attempt to algorithmically determine the chord represented by a list of notes by evaluating the intervals between them
     * @param notes -- notes to perform interval analysis on
     * @throws UnableToDetermineHarmonyException -- algorithmic detection failed
     */
    public Harmony(LinkedList<Note> notes) throws UnableToDetermineHarmonyException {

        LinkedList<Note.NoteName> noteNames = new LinkedList<>();

        for (Note n : notes) {
            if (!n.isRest() && !noteNames.contains(n.getName())) {
                noteNames.add(n.getName());
            }
        }

        LinkedList<Interval> pairs = new LinkedList<>();

        for (int i = 0; i < noteNames.size(); i++) {
            Note.NoteName firstNote = noteNames.get(i);
            for (int j = 0; j < noteNames.size(); j++) {
                Note.NoteName secondNote = noteNames.get(j);
                Interval pair = new Interval(firstNote, secondNote);
                pairs.add(pair);
            }
        }

        LinkedList<Interval> majorIntervals = new LinkedList<>();
        LinkedList<Interval> minorIntervals = new LinkedList<>();

        for (Interval pair : pairs) {
            if (pair.getInterval() == 3) {
                minorIntervals.add(pair);
            }
            if (pair.getInterval() == 4) {
                majorIntervals.add(pair);
            }
        }

        // Diminished chord

        if (minorIntervals.size() == 3) {
            rootNote = minorIntervals.get(0).getNote1();
            tonality = Tonality.MINOR;
            inversion = Inversion.A;
            dissonances = EnumSet.of(Dissonance.B5, Dissonance.ADDED7);
            return;
        }


        // Major or minor triad

        else if (majorIntervals.size() == 1 && minorIntervals.size() == 1) {
            if (minorIntervals.get(0).getNote2() == majorIntervals.get(0).getNote1()) {
                rootNote = minorIntervals.get(0).getNote1();
                tonality = Tonality.MINOR;
                inversion = Inversion.A;
                dissonances = EnumSet.noneOf(Dissonance.class);
                return;
            }
            if (majorIntervals.get(0).getNote2() == minorIntervals.get(0).getNote1()) {
                rootNote = majorIntervals.get(0).getNote1();
                tonality = Tonality.MAJOR;
                inversion = Inversion.A;
                dissonances = EnumSet.noneOf(Dissonance.class);
                return;
            }
        }

        // Dominant 7th or minor 7th

        else if (majorIntervals.size() == 1 && minorIntervals.size() == 2) {
            int lowerMinor = 0;
            boolean dominant = false;
            if (minorIntervals.get(0).getNote2() == minorIntervals.get(1).getNote1()) {
                lowerMinor = 0;
                dominant = true;
            } else if (minorIntervals.get(1).getNote2() == minorIntervals.get(0).getNote1()) {
                lowerMinor = 1;
                dominant = true;
            } else {
                if (minorIntervals.get(0).getNote2() == majorIntervals.get(0).getNote1()) {
                    lowerMinor = 0;
                } else if (minorIntervals.get(1).getNote2() == majorIntervals.get(0).getNote1()) {
                    lowerMinor = 1;
                }
            }
            if (dominant) {
                rootNote = majorIntervals.get(0).getNote1();
                tonality = Tonality.MAJOR;
                inversion = Inversion.A;
                dissonances = EnumSet.of(Dissonance.ADDED7);
                return;
            } else {
                rootNote = minorIntervals.get(lowerMinor).getNote1();
                tonality = Tonality.MINOR;
                inversion = Inversion.A;
                dissonances = EnumSet.of(Dissonance.ADDED7);
                return;
            }
        }


        throw new UnableToDetermineHarmonyException("Interval analysis failed.");

    }

    /**
     * Get the inversion of this harmony
     * @return - inversion
     */
    public Inversion getInversion() {
        return inversion;
    }

    /**
     * Setter for the inversion
     * @param inversion
     */
    public void setInversion (Inversion inversion) {
        this.inversion = inversion;
    }

    /**
     * Get the root note of this harmony
     * @return - root note
     */
    public Note.NoteName getRootNote() {
        return rootNote;
    }

    /**
     * Get the dissonances of this Harmony
     * @return dissonances
     */
    public EnumSet<Dissonance> getDissonances() {
        return dissonances;
    }

    /**
     * Get the tonality of this harmony
     * @return tonality
     */
    public Tonality getTonality() { return tonality; }

    /**
     * Randomises the root and tonality of this Harmony using the supplied PRNG
     * @param random - PRNG
     */
    public void randomise(Random random) {
        rootNote = Note.NoteName.get(random.nextInt(12));
        tonality = (random.nextBoolean()) ? Tonality.MAJOR : Tonality.MINOR;
//        inversion = Inversion.values()[random.nextInt(4)];
    }

    @Override
    /**
     * Overridden equality check for use in HashMap
     * Note: ignores dissonances and inversions, just compares the underlying triad
     * @param o -- object to compare with
     * @return - true/false
     */
    public boolean equals (Object o ) {
        if (this == o) return true;
        if (!(o instanceof Harmony)) return false;
        Harmony harmony = (Harmony) o;
        return (this.getRootNote() == harmony.getRootNote() && this.getTonality() == harmony.getTonality());
    }

    /**
     * Clone the harmony object
     * Used when cloning harmonic sequences in Genetic crossover operation
     * @return Cloned harmony
     * @throws CloneNotSupportedException
     */
    @SuppressWarnings({"CloneDoesntCallSuperClone"})
    @Override
    public Harmony clone() {
        return new Harmony(this.getRootNote(), this.getInversion(), EnumSet.copyOf(this.dissonances), this.getTonality());
    }

    @Override
    public String toString() {
        String result = "";
        result += this.rootNote;
        result += " ";
        result += this.tonality;
        return result;
    }

}
