package uk.ac.cam.ml692.bachnet;

import javax.sound.midi.*;
import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.*;

public class Chorale {

    private ArrayList<MelodicSequence> tracks;
    private HarmonicSequence harmonies;
    private KeySignature keySignature;
    private TimeSignature timeSignature;
    private Track originalMidiMetaTrack;

    /**
     * Construct an empty Chorale
     */
    public Chorale() {
        tracks = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            tracks.add(new MelodicSequence());
        }
        harmonies = new HarmonicSequence();
    }

    // TODO: Test this actually clones? Or maybe change to a function returning a Chorale? Where is it even used
    /**
     * Construct a chorale by copying an existing chorale
     * @param c
     */
    public Chorale(Chorale c) {
        tracks = (ArrayList<MelodicSequence>) c.tracks.clone();
    }

    /**
     * Constructor to construct a new chorale from two existing chorales and a split index
     * Ignores melody line
     * Assumes all notes in ATB lines are crotchets
     * This is for the crossover mutation function
     */
    public Chorale(Chorale chorale1, Chorale chorale2, int splitIndex) {

        // Preserve sop line
        this.addSopranoLine(chorale1.getSopranoLine());

        for (int i = 1; i < 4; i++) {

            MelodicSequence seq1 = chorale1.getTracks().get(i);
            MelodicSequence seq2 = chorale2.getTracks().get(i);

            MelodicSequence merged = new MelodicSequence();

            // They should be the same, but check just in case
            int highestBeat = Math.max(seq1.getHighestBeat(), seq2.getHighestBeat());

            for (int j = 0; j < highestBeat; j++) {
                if (j < splitIndex) {
                    merged.addNotes(seq1.getNote(j), j);
                } else {
                    merged.addNotes(seq2.getNote(j), j);
                }
            }

            this.tracks.set(i, merged);

        }

    }

    // TODO: Check preservation of meta track works
    /**
     * Construct a Chorale from a MIDI sequence
     * Will also preserve META track, if available
     * @param sequence
     */
    public Chorale(Sequence sequence) {

        tracks = new ArrayList<>(4);
        for (int i = 0; i < 4; i++) {
            tracks.add(new MelodicSequence());
        }

        double resolution = sequence.getResolution();

        int trackNumber = 0;
        int emptyTracks = 0;

        for (Track track :  sequence.getTracks()) {

            MelodicSequence melodicSequence = new MelodicSequence();

            int currentBeat = 0;
            double noteStarted = 0;
            double previousNoteEnded = 0;
            int numberOfNotes = 0;
            boolean isFirstInBeat = false;
            LinkedList<Note> currentBeatNotes = new LinkedList<>();

            for (int i=0; i < track.size(); i++) {

                MidiEvent event = track.get(i);
                MidiMessage message = event.getMessage();

                if (message instanceof ShortMessage) {

                    ShortMessage sm = (ShortMessage) message;

                    if (sm.getCommand() == MIDI.NOTE_ON && (sm.getData2() > 0)) { // Note started

                        // Remember when the note started for length calculation
                        noteStarted = (double) event.getTick();

                    } else {
                        if (sm.getCommand() == MIDI.NOTE_OFF || sm.getData2() == 0) { // Note finished

                            int pitch = sm.getData1();
                            double length = 4l * (event.getTick() - noteStarted) / resolution;

                            Note.NoteLength noteLength = Note.NoteLength.get((int) length);

                            Note noteToAdd = new Note(pitch, noteLength);

//                            System.out.println("Track " + trackNumber + " beat " + currentBeat + " note " + noteToAdd.getName());

                            // Current tick divided by ticks-per-beat rounded down
                            int thisBeat = (int) Math.floor(noteStarted / resolution);
                            // If new beat, update currentBeat, write away the previous currentBeatNotes and set first flag (for rest handling)
                            if (thisBeat > currentBeat) {
                                if (currentBeatNotes.size() > 0) {
                                    melodicSequence.addNotes(currentBeatNotes, currentBeat);
                                }
                                currentBeatNotes = new LinkedList<>();
                                isFirstInBeat = true;
                                currentBeat = thisBeat;
                            } else {
                                isFirstInBeat = false;
                            }

                            numberOfNotes++;

                            if (isFirstInBeat && ((noteStarted % resolution) != 0)) {
                                // First note in bar but there is a rest first
                                double restLength = 4l * (noteStarted % resolution) / resolution;
                                Note restToAdd = new Note(Note.NoteName.REST, Note.NoteLength.get((int) restLength), 4);
                                currentBeatNotes.add(restToAdd);
                                currentBeatNotes.add(noteToAdd);
                            } else if (isFirstInBeat && ((noteStarted % resolution) == 0)) {
                                // First note in bar and no rest
                                currentBeatNotes.add(noteToAdd);
                            } else if (!isFirstInBeat) {
                                // Need to check if we need a rest between previous note and this one
                                if (noteStarted - previousNoteEnded > 0) {
                                    // Need a rest
                                    double restLength = 4l * (event.getTick() - previousNoteEnded) / resolution;
                                    Note restToAdd = new Note(Note.NoteName.REST, Note.NoteLength.get((int) restLength), 4);
                                    currentBeatNotes.add(restToAdd);
                                    currentBeatNotes.add(noteToAdd);
                                } else {
                                    // No rest required, just add note
                                    currentBeatNotes.add(noteToAdd);
                                }
                            }

                            previousNoteEnded = event.getTick();

                        }
                    }
                } else if (message instanceof MetaMessage) {
                    // If it's an end of track message we should write away the notes currently in currentBeatNotes
                    if (((MetaMessage) message).getType() == 0x2F && (currentBeatNotes.size() > 0)) {
                        melodicSequence.addNotes(currentBeatNotes, currentBeat);
                    }
                }
            }

            if (numberOfNotes == 0) {
                if (trackNumber == 0) {
                    // This must be the meta track
                    originalMidiMetaTrack = track;
                }
                emptyTracks++;
            } else {
                if (trackNumber - emptyTracks > 3) {
                    System.out.println("Too many tracks with notes in input file. Just taking first four music lines but be careful.");
                    return;
                }
                tracks.set(trackNumber - emptyTracks, melodicSequence);
            }

            trackNumber++;

        }

    }

    /**
     * Returns the soprano line for this Chorale
     * @return
     */
    public MelodicSequence getSopranoLine() {
        return tracks.get(0);
    }

    /**
     * Converts a Chorale object to a MIDI sequence
     * @return
     * @throws InvalidMidiDataException
     */
    public Sequence toSequence() throws InvalidMidiDataException {

        // 5 if meta, 4 if not
        int outputTracks = (originalMidiMetaTrack == null) ? MIDI.outputNumTracks : MIDI.outputNumTracks+1;

        // Empty sequence
        Sequence mySequence = new Sequence(MIDI.outputDivisionType, MIDI.outputResolution, outputTracks);

        // Current MIDI track number
        int channelNumber = 0;

        // Preserve meta track if possible
        if (originalMidiMetaTrack != null) {
            mySequence.getTracks()[0] = originalMidiMetaTrack;
            channelNumber++;
        }

        // Velocity
        int data2 = 64;

        // Encode notes
        for(MelodicSequence m : tracks) {
            Track currentTrack = mySequence.getTracks()[channelNumber];
            TreeMap<Integer, LinkedList<Note>> trackNotes = m.getNotes();
            for (Map.Entry<Integer, LinkedList<Note>> entry : trackNotes.entrySet()) {
                int beatNumber = entry.getKey();
                double beatNumberInTicks = beatNumber * MIDI.outputResolution;
                double tickInBeat = 0;
                for (Note n : entry.getValue()) {
                    Note.NoteName nn = n.getName();
                    Note.NoteLength nl = n.getLength();
                    int pitch = n.getPitch();
                    double noteLengthInSemiQuavers = n.getLength().getValue();
                    double noteLengthInBeats = noteLengthInSemiQuavers/4.0;
                    double noteLengthInTicks = (noteLengthInBeats * MIDI.outputResolution);
                    double eventStart = beatNumberInTicks + tickInBeat;
                    double eventEnd = eventStart + noteLengthInTicks;

                    ShortMessage smOn = new ShortMessage(MIDI.NOTE_ON, channelNumber, pitch, data2);
                    ShortMessage smOff = new ShortMessage(MIDI.NOTE_OFF, channelNumber, pitch, 0);

                    MidiEvent midiEventNoteOn = new MidiEvent(smOn, (long) eventStart);
                    MidiEvent midiEventNoteOff = new MidiEvent(smOff, (long) eventEnd);

                    currentTrack.add(midiEventNoteOn);
                    currentTrack.add(midiEventNoteOff);

                    tickInBeat += noteLengthInTicks;
                }
            }
            channelNumber++;
        }

        return mySequence;

    }

    /** Returns all the parts as a collection of MelodicSequence objects
     * ArrayList for ordering: [S, A, T, B]
     * @return
     */
    public ArrayList<MelodicSequence> getTracks() {
        return tracks;
    }

    /**
     * Set the soprano line
     * @param sequence
     */
    public void addSopranoLine(MelodicSequence sequence) {
        tracks.set(0, sequence);
    }

    /**
     * Returns the harmonicSequence of this chorale, calculating from melody notes if needed
     * @return
     */
    public HarmonicSequence getHarmonies() {
        if (harmonies == null) harmonies = new HarmonicSequence(this);
        return harmonies;
    }

    /**
     * Updates the harmonicSequence of this chorale
     * @param harmonicSequence
     */
    public void setHarmonies(HarmonicSequence harmonicSequence) {
        harmonies = harmonicSequence;
    }

    /**
     * Attempt to determine the key signature of this chorale
     * @return
     */
    public KeySignature getKeySignature() throws UnableToDetermineKeySignatureException {
        if (keySignature == null) {
            keySignature = new KeySignature(this);
        }
        return keySignature;
    }

    /**
     * Get the time signature of the piece from the MIDI information, if possible
     * @return
     */
    public TimeSignature getTimeSignature() {
        if (timeSignature == null) {
            timeSignature = new TimeSignature(this.getOriginalMidiMetaTrack());
        }
        return timeSignature;
    }

    /**
     * Return the original MIDI meta information track, if available
     * @return
     */
    public Track getOriginalMidiMetaTrack() {
        return originalMidiMetaTrack;
    }

    /**
     * Normalises the Chorale to C Major (for NN)
     */
    public void normalise() {
        try {
            getKeySignature();
        } catch (UnableToDetermineKeySignatureException e) {
            System.out.println("Couldn't get KeySignature so cannot normalise. Leaving file untouched.");
            return;
        }
        if (keySignature.getRootNote() != Note.NoteName.C) {
            harmonies = null; // Reset HarmonicSequence

            int transposeBy = keySignature.getRootNote().getValue() - Note.NoteName.C.getValue();

            for (MelodicSequence melodicSequence : tracks) {
                melodicSequence.shift(transposeBy);
            }

            System.out.println("Transposed from " + keySignature.getRootNote() + " " + keySignature.getTonality() + " to C MAJOR (" + transposeBy + ").");

            // Calculate harmonies from scratch
            getHarmonies();
        }
    }

    /**
     * Helper function to empty the non-soprano lines from the chorale
     */
    public void emptyATBLines() {
        for (int i = 1; i < 4; i++) {
            tracks.get(i).getNotes().clear();
        }
    }

    /**
     * Overwrites existing melody lines with melodic representations of the harmonic sequence
     */
    public void harmonicSequenceToMelodicSequences() {

        getHarmonies();
        emptyATBLines();

        MelodicSequence bassLine = this.getTracks().get(3);
        MelodicSequence tenorLine = this.getTracks().get(2);
        MelodicSequence altoLine = this.getTracks().get(1);

        int bassOctave = 3;
        int tenorOctave = 4;
        int altoOctave = 5;

        for (Map.Entry<Integer, Harmony> harmonyEntry : harmonies.getHarmonies().entrySet()) {

            Harmony harmony = harmonyEntry.getValue();
            int currentBeat = harmonyEntry.getKey();

            // Root, third, fifth notes
            Note.NoteName root = harmony.getRootNote();
            Note.NoteName third;
            Note.NoteName fifth;

            // Calculate note values
            int interval = (harmony.getTonality() == Harmony.Tonality.MAJOR) ? 4 : 3;
            int thirdValue = root.getValue() + interval;
            int fifthValue = root.getValue() + 7;

            // Range check
            if (thirdValue > 11) thirdValue -= 12;
            if (fifthValue > 11) fifthValue -= 12;

            third = Note.NoteName.get(thirdValue);
            fifth = Note.NoteName.get(fifthValue);

            LinkedList<Note> bassNote = new LinkedList<>();
            LinkedList<Note> tenorNote = new LinkedList<>();
            LinkedList<Note> altoNote = new LinkedList<>();

            Harmony.Inversion inversion = harmony.getInversion();

            if (inversion == Harmony.Inversion.A) {
                bassNote.add(new Note(root, Note.NoteLength.C, bassOctave));
            } else if (inversion == Harmony.Inversion.B) {
                bassNote.add(new Note(third, Note.NoteLength.C, bassOctave));
                third = root;
            } else if (inversion == Harmony.Inversion.C) {
                bassNote.add(new Note(fifth, Note.NoteLength.C, bassOctave));
                fifth = root;
            } else {
                bassNote.add(new Note(root, Note.NoteLength.C, bassOctave));
            }

            if (Math.random() < 0.5) {
                tenorNote.add(new Note(third, Note.NoteLength.C, tenorOctave));
                altoNote.add(new Note(fifth, Note.NoteLength.C, altoOctave));
            } else {
                tenorNote.add(new Note(fifth, Note.NoteLength.C, tenorOctave));
                altoNote.add(new Note(third, Note.NoteLength.C, altoOctave));
            }

            bassLine.addNotes(bassNote, currentBeat);
            tenorLine.addNotes(tenorNote, currentBeat);
            altoLine.addNotes(altoNote, currentBeat);

            // TODO: Handle dissonances

        }

    }

    /**
     * Returns a linked list of the notes at a given beat across all melody lines
     * @param beat -- beat to collect notes from
     * @return
     */
    public LinkedList<Note> getHarmonyNotesAtBeat(int beat) {
        LinkedList<Note> notes = new LinkedList<>();
        for (int i = 1; i < 4; i++) {
            MelodicSequence sequence = tracks.get(i);
            if (sequence.hasNoteAtIndex(beat)) {
                LinkedList<Note> beatNotes = sequence.getNote(beat);
                Note firstNote = beatNotes.getFirst();
                if (firstNote.getName() != Note.NoteName.REST) {
                    notes.add(firstNote);
                }
            }
        }
        return notes;
    }

    // TODO: Stop introducing notes out of key

    /**
     * Function to add passing notes to a chorale generated by the Neural Nets
     * Rate is a double value from 0.0 to 1.0 that determines with what probability we add a passing note at a given opportunity
     * @param rate
     */
    public void addPassingNotes(double rate) {

        if (rate > 1 || rate < 0) {
            System.out.println("Unable to add passing notes -- rate spurious");
            return;
        }

        for (MelodicSequence melodicSequence : tracks) {

            // Skip soprano line - there's undoubtedly a better way of doing this
            if (tracks.indexOf(melodicSequence) != 0) {

                LinkedList<Note> previousNotes = null;

                for (int i = 0; i < melodicSequence.getNotes().size(); i++) {
                    if (melodicSequence.hasNoteAtIndex(i) && melodicSequence.hasNoteAtIndex(i+1)) {
                        LinkedList<Note> thisNotes = melodicSequence.getNote(i);
                        LinkedList<Note> nextNotes = melodicSequence.getNote(i+1);

                        if (thisNotes.size() == 1 && thisNotes.get(0).getLength() == Note.NoteLength.C && nextNotes.get(0).getName() != Note.NoteName.REST) {

                            int thisPitch = thisNotes.get(0).getPitch();
                            int nextPitch = nextNotes.get(0).getPitch();
                            int amountToAdjust = 2;

                            int difference = nextPitch - thisPitch;

                            if (difference < 0) {
                                difference *= -1;
                                amountToAdjust = -2;
                            }

                            if (difference == 3 || difference == 4) {

                                if (difference == 3) {
                                    if (amountToAdjust > 0) {
                                        amountToAdjust *= 0.5;
                                    }
                                }

                                if (Math.random() <= rate) {
                                    Note noteToSplice = thisNotes.get(0);
                                    Note newNote = new Note(thisPitch + amountToAdjust, Note.NoteLength.Q);
                                    Note halvedNote = new Note(noteToSplice.getPitch(), Note.NoteLength.Q);
                                    thisNotes.set(0, halvedNote);
                                    thisNotes.add(newNote);
                                    System.out.println("Passing note added at beat " + i + " on track " + tracks.indexOf(melodicSequence));
                                }

                            }

                        }

                    }
                }

            }

        }

    }

}
